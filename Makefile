# docs: https://www.gnu.org/software/make/manual/

APP = "nestjs-boilerplate"
DATABASE = "nestjs-boilerplate-database"

# setup environment user
SHELL = /bin/sh

CURRENT_UID := $(shell id -u)
CURRENT_GID := $(shell id -g)

export CURRENT_UID
export CURRENT_GID

# setup
env:
	cp .env.example .env
.PHONY: env

# build
database:
	docker-compose up -d $(DATABASE)
.PHONY: database

app:
	docker-compose up -d $(APP)
.PHONY: app

all: database app
.PHONY: all

# clean
down:
	docker-compose rm -f -s -v
.PHONY: down

# tests
test:
	docker-compose exec $(APP) yarn test
.PHONY: test

test-cov:
	docker-compose exec $(APP) yarn test:cov -i
.PHONY: test-coverage

test-e2e:
	docker-compose exec $(APP) yarn test:e2e
.PHONY: test-e2e

# misc
logs:
	docker-compose logs -f $(APP)
.PHONY: logs

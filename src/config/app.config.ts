import 'dotenv/config';
import { env } from 'node:process';

export const appConfig = {
  appName: env.npm_package_name,
  appDescription: env.npm_package_description,
  appVersion: env.npm_package_version,
};

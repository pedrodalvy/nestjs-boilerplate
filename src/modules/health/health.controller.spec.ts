import { Test, TestingModule } from '@nestjs/testing';
import { HealthController } from './health.controller';
import { mock } from 'jest-mock-extended';
import { HealthService } from './services/health.service';
import { HealthCheckDTO } from './dto/health-check.dto';

const mockHealthService = mock<HealthService>();

describe('HealthController', () => {
  let sut: HealthController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [{ provide: HealthService, useValue: mockHealthService }],
      controllers: [HealthController],
    }).compile();

    sut = module.get<HealthController>(HealthController);
  });

  describe('check', () => {
    it('should return health check message', () => {
      const message: HealthCheckDTO = {
        message: 'Service is up and running.',
      };

      jest.spyOn(mockHealthService, 'check').mockReturnValue(message);

      expect(sut.check()).toEqual(message);
    });
  });
});

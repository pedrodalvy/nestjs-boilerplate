import { Controller, Get } from '@nestjs/common';
import { HealthService } from './services/health.service';
import { HealthCheckDTO } from './dto/health-check.dto';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';

@Controller('health')
@ApiTags('Api')
export class HealthController {
  constructor(private readonly healthService: HealthService) {}

  @Get('check')
  @ApiOperation({ summary: 'Health check' })
  @ApiOkResponse({ type: HealthCheckDTO })
  check(): HealthCheckDTO {
    return this.healthService.check();
  }
}

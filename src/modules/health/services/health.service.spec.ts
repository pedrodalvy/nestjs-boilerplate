import { Test, TestingModule } from '@nestjs/testing';
import { HealthService } from './health.service';
import { HealthCheckDTO } from '../dto/health-check.dto';

describe('ExampleService', () => {
  let sut: HealthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HealthService],
    }).compile();

    sut = module.get<HealthService>(HealthService);
  });

  describe('check', () => {
    it('should return health check message', () => {
      const message: HealthCheckDTO = {
        message: 'Service is up and running.',
      };

      expect(sut.check()).toEqual(message);
    });
  });
});

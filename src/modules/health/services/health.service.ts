import { Injectable } from '@nestjs/common';
import { HealthCheckDTO } from '../dto/health-check.dto';

@Injectable()
export class HealthService {
  check(): HealthCheckDTO {
    return { message: 'Service is up and running.' };
  }
}

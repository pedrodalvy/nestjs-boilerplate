# NestJS Boilerplate

This is a boilerplate for NestJS projects.

## Getting Started

Just use this template and start coding!

## Checklist

Follow this checklist to update your project info.

- [ ] Create `.env` file: run `make env`
- [ ] Update `.env` file with your project info
- [ ] Update `Makefile` with your docker images names

## Running the app
```bash
# Docker
$ make all

# yarn
$ yarn start
```

## Test

```bash
# unit tests
$ make test

# e2e tests
$ make test-e2e

# test coverage
$ make test-cov
```

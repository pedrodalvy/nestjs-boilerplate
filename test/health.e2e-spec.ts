import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import * as request from 'supertest';

describe('Health - /health', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('/check (GET)', () => {
    it('should return 200', () => {
      return request(app.getHttpServer()).get('/health/check').expect(200);
    });

    it('should return a success message', () => {
      return request(app.getHttpServer())
        .get('/health/check')
        .expect({ message: 'Service is up and running.' });
    });
  });
});
